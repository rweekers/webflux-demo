package nl.craftsmen.webflux

import com.mongodb.reactivestreams.client.MongoClients
import nl.craftsmen.webflux.domain.ChatMessage
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.mongodb.core.CollectionOptions
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import reactor.core.publisher.Mono

@SpringBootApplication
class WebfluxApplication {

    @Bean
    fun init(mongoOperations: MongoOperations) = CommandLineRunner {
        val mongo = MongoClients.create("mongodb://localhost");
        val template = ReactiveMongoTemplate(mongo, "local")
        val chatMessageCollectionName = template.getCollectionName(ChatMessage::class.java)
        mongoOperations.createCollection(chatMessageCollectionName, CollectionOptions.empty().capped().size(9999999L))

        val chats = Mono.just(
                listOf(
                        ChatMessage("1", "Paul", "Hi there!"),
                        ChatMessage("2", "Ringo", "Hi yourself!"),
                        ChatMessage("3", "George", "Hello"),
                        ChatMessage("4", "John", "Hi"),
                        ChatMessage("5", "Paul", "I have a great idea for a song!")
                )
        )

        template.insertAll(chats).subscribe()
    }

}

fun main(args: Array<String>) {
    runApplication<WebfluxApplication>(*args)
}
