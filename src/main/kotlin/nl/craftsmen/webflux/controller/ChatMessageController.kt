package nl.craftsmen.webflux.controller

import nl.craftsmen.webflux.domain.ChatMessage
import nl.craftsmen.webflux.repository.ChatMessageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/api")
class ChatMessageController {

    @Autowired
    private lateinit var chatMessageRepository: ChatMessageRepository

    @CrossOrigin(origins = ["*"])
    @GetMapping("/chats")
    fun allChatMessages(): Flux<ChatMessage> =
        chatMessageRepository.findWithTailableCursorBy()

    @CrossOrigin(origins = ["*"])
    @PostMapping("/chats")
    fun addChatMessage(@RequestBody message: ChatMessage): Mono<ChatMessage> =
        chatMessageRepository.save(message)

}