package nl.craftsmen.webflux.repository

import nl.craftsmen.webflux.domain.ChatMessage
import org.springframework.data.mongodb.repository.Tailable
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux

interface ChatMessageRepository : ReactiveCrudRepository<ChatMessage, String> {

    @Tailable
    fun findWithTailableCursorBy(): Flux<ChatMessage>

}