package nl.craftsmen.webflux.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
class ChatMessage(
        @Id
        var id: String?,
        val user: String,
        val text: String)
