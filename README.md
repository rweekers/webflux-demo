# Webflux demo

Small Spring Webflux demo application for a chat application. Simply start the application locally and communicate with the endpoints (by using https://gitlab.com/rweekers/webflux-demo-client for example).